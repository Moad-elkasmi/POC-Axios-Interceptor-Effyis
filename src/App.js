import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { Provider } from 'react-redux'
import store from 'utils/store'
import 'utils/ignore'
// api-client.js



// assets
import { imageAssets } from 'theme/images'
import { fontAssets } from 'theme/fonts'
import Navigator from './navigator'
import { QueryClient} from 'react-query'
// import { useGetLogin } from './utils/useGetLogin'



const App = () => {
  const [didLoad, setDidLoad] = useState(false)

  // assets preloading
  const handleLoadAssets = async () => {
    await Promise.all([...imageAssets, ...fontAssets])
    setDidLoad(true)
  }
  // const state = store.getState();
  // const header = {
  //   "JWT": state.app.jwt,
  //   "API-KEY": state.app.apiKey,
  //   "SECRET-Key": state.app.secretKey
  // };
  useEffect(() => {
    handleLoadAssets()
    // console.log(data, error)
    // instance.get(`/banks`)
  }, [])
  // const { data, error } = useGetLogin()
  // const wrapHeader = (jwt)=>{
  //   dispatch(saveMe({ jwt:jwt, secretKey:'SECRET-KEY2', apiKey:'API-KEY2' }))
  // }

  //  const instance = axios.create({

  //   baseURL: `https://engin.effyispayment.com/switch-service`,
  // });

  //   instance.interceptors.request.use(
  //     config => {
  //       // console.log("request-sucess", config);


  //       config.headers = header;
  //       return config
  //     },
  //     error => {
  //       console.log("request Error", error);
  //       return Promise.reject(error);
  //     }
  //   );
  //   instance.interceptors.response.use(
  //     response => {
  //       if (response.status == 200) {
  //         let newAccountScope = '123'
  //         // newAccountScope = response.data[1].accountScope
  //        wrapHeader(newAccountScope)

  //         console.log(response.config.headers);
  //       };
  //     },
  //     error => {
  //       console.log("response Error", error);
  //       return Promise.reject(error);
  //     }
  //   );
  const queryClient = new QueryClient();
  // const { dispatch } = store;
  return didLoad ? (
      <Provider store={store}>
        <Navigator />
      </Provider>
  ) : (
    <View />
  )

};
export default App
