// // api-client.js
import axios from 'axios'
import { saveMe } from './slices/app.slice';


const instance = axios.create({
    baseURL: `https://engin.effyispayment.com/switch-service`,
});



export default function setupAxios(store, isGet, url) {
    if (isGet) {
        instance.get(url)
        // store.dispatch(saveMe({ jwt: "65456 ", apiKey:"ka-me-ha-me-ha" }))
        const state = store.getState()
        // console.log("Hi from Interceptor", state)
        

        instance.interceptors.request.use(config => {
            
            return config

        }
            ,
            error => {
                return Promise.reject(error);
            }
        );
        instance.interceptors.response.use(

            response => {
                response
                //     if (response.status == 200) {
                //         var newAccountScope = ''
                //         newAccountScope = response.data[1].accountScope
                //         // console.log(response.config.headers);
                //     };
            },
            error => {
                console.log(error);
            }
        );
    }
    // const state = store.getState();
    // const header = {
    //     "JWT": state.app.jwt,
    //     "API-KEY": state.app.apiKey,
    //     "SECRET-Key": state.app.secretKey
    // };
}




