/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'

// ------------------------------------
// Constants
// ------------------------------------

const initialState = {
  checked: false,
  loggedIn: false,
  jwt: "",
  apiKey:"",
  secretKey : ""
}

// ------------------------------------
// Slice
// ------------------------------------

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    authenticate: (state, { payload }) => {
      state.loggedIn = payload.loggedIn
      state.checked = payload.checked
    },
    saveMe: (state, { payload }) => {
      state.jwt = payload.jwt,
      state.apiKey = payload.apiKey,
      state.secretKey = payload.secretKey
    }
    
  },
})




export const { action } = appSlice
export const { authenticate, saveMe } = appSlice.actions

export default appSlice.reducer
