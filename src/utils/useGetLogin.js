import axios from "axios"
import { useQuery } from "react-query"

const GetLogin = () => {
    return axios.get(`https://engin.effyispayment.com/switch-service`)
}
export const useGetLogin = () =>{
    return useQuery('get-login', 
    GetLogin,
        //onSucess & onError
    )
}